package com.afpa.u;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmpBddDaoImpl implements Dao<Emp> {
	public void remove(Emp emp) {
		// TODO Auto-generated method stub
	}
	public Emp update(Emp emp) {
		// TODO Auto-generated method stub
		return null;
	}
	public List<Emp> getAll() {
		Connection c = MyConnection.getConnection();
		List<Emp> res = null;
		try {
			PreparedStatement selectAllPs = c.prepareStatement("SELECT * FROM emp;");
			ResultSet result = selectAllPs.executeQuery();
			res = new ArrayList<>();
			while (result.next()) {
				// on indique chaque fois le nom de la colonne et le type
				int idemp = result.getInt("num");
				String nom = result.getString("nom");
				String prenom = result.getString("prenom");
				// pareil pour tous les autres attributs
				Emp p = new Emp(nom, prenom);
				p.setNum(idemp);
				res.add(p);
			}
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public Emp save(Emp emp) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("insert into emp (nom,prenom) values (?,?); ", PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, emp.getNom());
				ps.setString(2, emp.getPrenom());
				ps.executeUpdate();
				ResultSet resultat = ps.getGeneratedKeys();
				if (resultat.next()) {
					emp.setNum(resultat.getInt(1));
					return emp;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	public Emp findById(int id) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("select * from emp where num = ?; ");
				ps.setInt(1, id);
				ResultSet r =ps.executeQuery();
				if (r.next())
					return new Emp(r.getInt("num"),r.getString("nom"),r.getString("prenom"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}