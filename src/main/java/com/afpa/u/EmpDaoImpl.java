package com.afpa.u;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EmpDaoImpl implements EmpDao {
	public void remove(Emp emp) {
		// TODO Auto-generated method stub
	}

	public Emp update(Emp emp) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Emp> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Emp save(Emp emp) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("insert into Emp (noEmp,nom,prenom) values (?,?,?); ");
				ps.setString(2, emp.getNom());
				ps.setString(3, emp.getPrenom());
				ps.setInt(1, emp.getNum());
				ps.executeUpdate();
//				ResultSet resultat = ps.getGeneratedKeys();
//				if (resultat.next()) {
//					Emp.setNum(resultat.getInt(1));
//					return Emp;
//				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return emp;
	}

	public Emp findById(int id) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("select * from Emp where noEmp = ?; ");
				ps.setInt(1, id);
				ResultSet r = ps.executeQuery();
				if (r.next())
					return new Emp(r.getInt("noemp"), r.getString("nom"), r.getString("prenom"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}