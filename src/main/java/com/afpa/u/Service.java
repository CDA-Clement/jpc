package com.afpa.u;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class Service{
	private int noserv;
	@NonNull
	private String service;
	@NonNull
	private String ville;	
}