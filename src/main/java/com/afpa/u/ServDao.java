package com.afpa.u;

import java.util.List;

public interface ServDao {
	Service save(Service personne);
	void remove(Service personne);
	Service update(Service personne);
	Service findById(int id);
	List<Service> getAll();
}