package com.afpa.u;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

public class MyConnection {
	private static Connection connexion = null;

	public static void stop() {
		if (connexion != null) {
			try {
				connexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
//
//	private MyConnection() {
//		DataSource dataSource = MyDataSourceFactory.get;
//		try {
//			connexion = dataSource.getConnection();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	public synchronized static Connection getConnection() {
		if (connexion == null) {
			new MyConnection();
		}
		return connexion;
	}
}