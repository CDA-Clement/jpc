package com.afpa.u;

import java.util.List;

public interface EmpDao {
	Emp save(Emp personne);
	void remove(Emp personne);
	Emp update(Emp personne);
	Emp findById(int id);
	List<Emp> getAll();
}