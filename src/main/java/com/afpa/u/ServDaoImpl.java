package com.afpa.u;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ServDaoImpl implements ServDao {
	public void remove(Service serv) {
		// TODO Auto-generated method stub
	}

	public Service update(Service serv) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Service> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Service save(Service serv) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("insert into Service (noserv,service,ville) values (?,?,?); ");
				ps.setString(2, serv.getService());
				ps.setString(3, serv.getVille());
				ps.setInt(1, serv.getNoserv());
				ps.executeUpdate();
//				ResultSet resultat = ps.getGeneratedKeys();
//				if (resultat.next()) {
//					Service.setNum(resultat.getInt(1));
//					return Service;
//				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return serv;
	}

	public Service findById(int id) {
		Connection c = MyConnection.getConnection();
		if (c != null) {
			try {
				PreparedStatement ps = c.prepareStatement("select * from Service where noEmp = ?; ");
				ps.setInt(1, id);
				ResultSet r = ps.executeQuery();
				if (r.next())
					return new Service(r.getInt("noserv"), r.getString("service"), r.getString("ville"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}