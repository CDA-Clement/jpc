package com.afpa.jdbc;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.afpa.u.Emp;
import com.afpa.u.EmpDaoImpl;
import com.afpa.u.ServDaoImpl;
import com.afpa.u.Service;

public class Menu {
	private static Logger monLogger = LoggerFactory.getLogger(Menu.class);
	private static Connection connection;
	private static boolean authentifie = false;
	private static PreparedStatement insertionPS;
	private static PreparedStatement insertionServicePS;
	private static PreparedStatement selectAllPs;
	private static PreparedStatement loginPs;
	private static PreparedStatement recupIdPs;
	private static PreparedStatement recupIdServicePs;
	private static PreparedStatement selectAllServicePs;
	private static PreparedStatement selectAllService2Ps;
	private static PreparedStatement modifierSalairePs;
	private static PreparedStatement saliareMinParService;
	private static int id;
	private static int idService;

	private static void initConnection() throws Exception {
		String url = "jdbc:postgresql://localhost:5432/jpc";
		String user = "user1";
		String password = "pwd1";
		connection = DriverManager.getConnection(url, user, password);

		insertionPS = connection.prepareStatement("INSERT INTO emp (noemp,nom,prenom) VALUES (?,?,?);");
		insertionServicePS = connection.prepareStatement("INSERT INTO serv (noserv,service,ville) VALUES (?,?,?);");
		selectAllPs = connection.prepareStatement("SELECT * FROM emp where noserv=?;");
		selectAllServicePs = connection.prepareStatement("SELECT * FROM serv;");
		selectAllService2Ps = connection.prepareStatement("SELECT * FROM emp WHERE noserv=?");
		loginPs = connection.prepareStatement("select * from emp where nom = ? ;");
		recupIdPs = connection.prepareStatement("select max(noemp)+1 as ID from emp;");
		recupIdServicePs = connection.prepareStatement("select max(noserv)+1 as ID from serv;");
		modifierSalairePs = connection.prepareStatement("update emp set sal=? where noemp=?;");
		saliareMinParService = connection.prepareStatement("");
	}
/**
 * Permet de recupérer la derniere id de la table emp
 * @return la derniere valeur de la table emp
 */
	private static int recupId() {
		int nbPersonne = 0;
		try {
			// Ex´ecution de la requˆete
			ResultSet result = recupIdPs.executeQuery();
			for (; result.next();) {
				nbPersonne = ((Number)result.getObject(1)).intValue();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nbPersonne;
	}

	private static int recupIdService() {
		int nbPersonne = 0;
		try {
			// Ex´ecution de la requˆete
			ResultSet result = recupIdServicePs.executeQuery();
			for (; result.next();) {
				nbPersonne = ((Number)result.getObject(1)).intValue();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nbPersonne;
	}

	 static void ajouterUnePersonne(Scanner sc) throws Exception {

		id = recupId();
		System.out.println("ajout personne : ");
		System.out.print(" saisir nom >");
		String nom = sc.nextLine();
		System.out.print(" saisir prenom >");
		String prenom = sc.nextLine();
		EmpDaoImpl empDaoImpl = new EmpDaoImpl();
		Emp personne = new Emp(id, nom, prenom);
		Emp insertedPersonne = empDaoImpl.save(personne);
		if (insertedPersonne != null) {
			System.out.println("personne numéro " + insertedPersonne.getNum() + " a été insérée");
		} else {
			System.out.println("problème d’insertion");
		}
		insertionPS.setInt(1, id);
		insertionPS.setString(2, nom);
		insertionPS.setString(3, prenom);

		int nbr = insertionPS.executeUpdate();
		if (0 != nbr)
			monLogger.debug("Insertion réussie");
			System.out.println("insertion réussie");
	}

	private static void afficherPersonnes(Scanner sc) throws Exception {
		System.out.print(" saisir n° service >");
		int noserv = sc.nextInt();
		sc.nextLine();
		selectAllPs.setInt(1, noserv);
		ResultSet result = selectAllPs.executeQuery();
		while (result.next()) {
			// on indique chaque fois le nom de la colonne et le type
			int idPersonne = result.getInt("noemp");

			String nom = result.getString("nom");

			String prenom = result.getString("prenom");

			// pareil pour tous les autres attributs
			System.out.println(idPersonne + " " + nom + " " + prenom);
		}
		monLogger.debug("Affichage réussie");
		System.out.println();
	}

	private static void afficherServices() throws Exception {

		ResultSet result = selectAllServicePs.executeQuery();
		while (result.next()) {
			// on indique chaque fois le nom de la colonne et le type
			int noserv = result.getInt("noserv");

			String service = result.getString("service");

			String ville = result.getString("ville");
			int u =0;
			selectAllService2Ps.setInt(1, noserv);
			ResultSet result2 = selectAllService2Ps.executeQuery();
			while(result2.next()) {
				u++;
			}


			// pareil pour tous les autres attributs
			System.out.println(noserv + " " + service + " " + ville + " " + u);
		}
		monLogger.debug("Affichage réussie");
		System.out.println();
	}

	private static void ajouterUnService(Scanner sc) throws Exception {
		idService = recupIdService();
		System.out.println("ajout service : ");
		System.out.print(" saisir service >");
		String nomService = sc.nextLine();
		System.out.print(" saisir ville >");
		String ville = sc.nextLine();
		ServDaoImpl servDaoImpl = new ServDaoImpl();
		Service service = new Service(idService, nomService, ville);
		Service insertedService = servDaoImpl.save(service);
		if (insertedService != null) {
			System.out.println("service numéro " + insertedService.getNoserv() + " a été insérée");
		} else {
			System.out.println("problème d’insertion");
		}
		insertionServicePS.setInt(1, idService);
		insertionServicePS.setString(2, nomService);
		insertionServicePS.setString(3, ville);

		int nbr = insertionServicePS.executeUpdate();
		if (0 != nbr)
			monLogger.debug("Insertion réussie");
			System.out.println("insertion réussie");
	}



	private static void modifierSalaire(Scanner sc) throws Exception {
		System.out.println("modifier salaire : ");
		System.out.print(" saisir ID employe >");
		String Id = sc.nextLine();
		Integer id=0;
		try {
			id=Integer.parseInt(Id);
		} catch (Exception e) {
			System.out.println("veuillez entrer un num");
		}

		System.out.print(" saisir salaire employe >");
		String salaire = sc.nextLine();
		Float sal=0f;
		try {
			sal=Float.parseFloat(salaire);
		} catch (Exception e) {
			System.out.println("veuillez entrer un salaire");
		}


		 modifierSalairePs.setFloat(1, sal);
		modifierSalairePs.setInt(2, id);
		int nbr = modifierSalairePs.executeUpdate();
		if (0 != nbr)
			monLogger.debug("Insertion réussie");
			System.out.println("salaire modifie");
	}

	public static void main(String[] args) throws Exception {
		exec(System.in);
	}
	
	public static void exec(InputStream in) {
		final List<Integer> actionsPrivees = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7);
		final List<Integer> actionsPubliques = Arrays.asList(0, 8);

		try {
			initConnection();

			Scanner sc = new Scanner(in);

			int choix = 0;
			boolean continuer = true;

			while (continuer) {
				System.out.println();

				if (authentifie) {
					System.out.println("0- arrêter le programme");
					System.out.println("1- ajouter un employé");
					System.out.println("2- modifier le salaire d'un employé");
					System.out.println("3- ajouter un service");
					System.out.println("4- lister les employés d'un service (le numero est à saisir)");
					System.out.println("5- lister les services avec le nombre d'employés par service");
					System.out.println("6- afficher le plus haut salaire pour chaque service");
					System.out.println("7- afficher le plus bas salaire pour chaque service.");
				} else {
					System.out.println("0- arrêter le programme");
					System.out.println("8- login");
				}

				System.out.print("> ");

				choix = sc.nextInt();
				sc.nextLine();

				if ((authentifie && !actionsPrivees.contains(choix))
						|| (!authentifie && !actionsPubliques.contains(choix))) {
					monLogger.info("Authentication failed");
					choix = -1;
				}

				switch (choix) {
				case 0:
					System.out.println("au revoir !");
					continuer = false;
					break;
				case 1:
					ajouterUnePersonne(sc);
					break;
				case 2:
					modifierSalaire(sc);
					break;
				case 3:
					ajouterUnService(sc);
					break;
				case 4:
					afficherPersonnes(sc);
					break;
				case 5:
					afficherServices();
					break;
				case 6:
					System.out.println("Command not yet");
					break;
				case 7:
					System.out.println("Command not yet");
					break;
				case 8:
					login(sc);
					break;
				default:
					System.out.println("action invalide !");
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private static void login(Scanner sc) throws Exception {
		System.out.println("entrez votre nom : ");
		System.out.print(">");
		String nom = sc.nextLine();

		loginPs.setString(1, nom);

		ResultSet result = loginPs.executeQuery();

		System.out.println(loginPs);

		if (result.next()) {
			authentifie = true;
			System.out.println("auth reussie !");
		} else {
			System.out.println("mauvais login mot de passe !!");
		}
	}

}
